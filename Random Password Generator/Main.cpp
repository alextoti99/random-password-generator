#include "Generator.h"
#include "Help.h"
#include "Color.h"
#include <iostream>

int main()
{
	SetColor();
	Help();
	GeneratePassword();
	std::cout << "" << std::endl;
	std::cout << "Thanks for using the Random Password Generator" << std::endl;
	system("pause");
	return 0;
}
