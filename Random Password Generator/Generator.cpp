#include "Generator.h"
#include <windows.h>
#include <string>
#include <iostream>
#include <fstream>
#include<cstdlib>
#include <string>
#include<conio.h>
#include<stdlib.h>
#include <ctime> 

using namespace std;

void GeneratePassword()
{
	int size = 10;
	int choice = 1;
	cout << "" << endl;
	cout << "Choose the size of the password" << endl;
	cout << "" << endl;
	cin >> size;
	cout << "" << endl;
	srand(time(0)); 
	std::string result;
	cout << "Password Types: 1 (FullPasswrd), 2 (SmallLettersOnly), 3 (CapsOnly), 4 (SmallACapsANumbers), 5(SmallACapsASymbols)" << endl;
	cout << "" << endl;
	cin >> choice;
	cout << "" << endl;
	//Types
	const std::string chars1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+/\|";	
	const std::string chars2 = "abcdefghijklmnopqrstuvwxyz";
	const std::string chars3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	const std::string chars4 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	const std::string chars5 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_=+/\|";
	switch (choice)
	{
	case 1:
		for (size_t i = size; i; --i) {
			result.push_back(chars1[rand() % chars1.size()]);
		}
		break;
	case 2:
		for (size_t i = size; i; --i) {
			result.push_back(chars2[rand() % chars2.size()]);
		}
		break;
	case 3:
		for (size_t i = size; i; --i) {
			result.push_back(chars3[rand() % chars3.size()]);
		}
		break;
	case 4:
		for (size_t i = size; i; --i) {
			result.push_back(chars4[rand() % chars4.size()]);
		}
		break;
	case 5:
		for (size_t i = size; i; --i) {
			result.push_back(chars5[rand() % chars5.size()]);
		}
		break;
	default:

		for (size_t i = size; i; --i) {
			result.push_back(chars1[rand() % chars1.size()]);
		}
}
	
	ofstream file;
	file.open("password.txt");
	file << result;
	file.close();
}